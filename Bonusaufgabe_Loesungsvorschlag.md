### SSH-Agent und Systemd

#### Ziel
Ermöglichen Sie einem Systemd-Service den Zugriff auf einen SSH-Agenten, um einen SSH-Schlüssel mit Passphrase zu verwenden.

#### Anforderungen

1. **SSH-Agent**: Starten Sie einen SSH-Agenten und fügen Sie den SSH-Schlüssel hinzu.
2. **Systemd Environment**: Konfigurieren Sie die Systemd-Service-Datei, um die Umgebungsvariablen des SSH-Agenten zu nutzen.

#### Schritte

##### SSH-Agent starten und SSH-Schlüssel hinzufügen

Starten Sie den SSH-Agenten und fügen Sie den SSH-Schlüssel hinzu:

```bash
eval $(ssh-agent -s)
ssh-add /path/to/private_key
```

Notieren Sie die Ausgabe des `ssh-agent -s` Befehls. Sie enthält die Umgebungsvariablen `SSH_AUTH_SOCK` und `SSH_AGENT_PID`.

##### Systemd Service anpassen

Fügen Sie die Umgebungsvariablen in die Systemd-Service-Datei `backup.service` ein:

```ini
[Unit]
Description=Backup service

[Service]
Type=oneshot
ExecStart=/path/to/backup.sh
User=nobody
Group=nobody
Environment="SSH_AUTH_SOCK=/tmp/ssh-xxxxxx/agent.xxxx"
Environment="SSH_AGENT_PID=xxxx"
PrivateTmp=true
```

Ersetzen Sie die Platzhalter `xxxxxx` und `xxxx` durch die Werte, die Sie von `ssh-agent -s` erhalten haben.

#### Hinweise

- Diese Methode ist nicht ideal für Produktionsumgebungen, da der SSH-Agent und die Umgebungsvariablen manuell verwaltet werden müssen.
- Ein sichererer Ansatz wäre die Verwendung von speziellen Tools wie `keychain`, die den SSH-Agenten für Systemd-Dienste zugänglich machen können.
- Ändern Sie die Pfade und Variablen entsprechend Ihrer Systemkonfiguration.
