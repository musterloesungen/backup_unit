### Musterlösung

#### Minimales Bash-Script
```bash
#!/bin/bash
# Prüfen, ob der Remote-Rechner erreichbar ist
ping -c 1 remote_host > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Remote host is not reachable. Exiting."
  exit 1
fi

# Backup und SCP
tar czf - /path/to/source/ | ssh -i /path/to/private_key user@remote_host "cat > /path/to/remote/backup_$(date +%Y%m%d%H%M%S).tar.gz"
```

#### Erweitertes Bash-Script
```bash
#!/bin/bash

# Funktion zur Festlegung der Variablen
set_var() {
  local arg_value="$1"
  local env_value="$2"
  local default_value="$3"

  if [ -n "$arg_value" ]; then
    echo "$arg_value"
  elif [ -n "$env_value" ]; then
    echo "$env_value"
  else
    echo "$default_value"
  fi
}

# Variablen aus Kommandozeilenargumenten lesen
while getopts l:r:d:k: option; do
  case "${option}" in
    l) arg_local_backup_dir=${OPTARG};;
    r) arg_remote_host=${OPTARG};;
    d) arg_remote_dir=${OPTARG};;
    k) arg_private_key=${OPTARG};;
  esac
done

# Variablen festlegen
local_backup_dir=$(set_var "$arg_local_backup_dir" "$ENV_LOCAL_BACKUP_DIR" "/var/backup/eigenesVerzeichnis")
remote_host=$(set_var "$arg_remote_host" "$ENV_REMOTE_HOST" "user@remote_host")
remote_dir=$(set_var "$arg_remote_dir" "$ENV_REMOTE_DIR" "/path/to/remote")
private_key=$(set_var "$arg_private_key" "$ENV_PRIVATE_KEY" "/path/to/private_key")

# Prüfen, ob das lokale Backup-Verzeichnis existiert, sonst erstellen
if [ ! -d "$local_backup_dir" ]; then
  mkdir -p "$local_backup_dir"
  logger -t backup_script "Lokales Backup-Verzeichnis erstellt: $local_backup_dir"
fi

# Prüfen, ob der Remote-Rechner erreichbar ist
ping -c 1 "${remote_host%%@*}" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  remote_reachable=true
  logger -t backup_script "Remote-Rechner ist erreichbar."
else
  remote_reachable=false
  logger -t backup_script "Remote-Rechner ist nicht erreichbar. Lokales Backup wird erstellt."
fi

# Backup erstellen
backup_file="$local_backup_dir/backup_$(date +%Y%m%d%H%M%S).tar.gz"
tar czf "$backup_file" /path/to/source/
if [ $? -eq 0 ]; then
  logger -t backup_script "Backup erfolgreich erstellt: $backup_file"
else
  logger -t backup_script "Fehler beim Erstellen des Backups."
  exit 1
fi

# Vorherige Backups übertragen, wenn Remote-Rechner erreichbar
if [ "$remote_reachable" = true ]; then
  for file in "$local_backup_dir"/*.tar.gz; do
    scp -i "$private_key" "$file" "$remote_host:$remote_dir"
    if [ $? -eq 0 ]; then
      logger -t backup_script "Backup erfolgreich übertragen: $file"
      rm -f "$file"
    else
      logger -t backup_script "Fehler beim Übertragen des Backups: $file"
    fi
  done
fi
```

#### Systemd Service
```ini
[Unit]
Description=Backup service

[Service]
Type=oneshot
ExecStart=/path/to/backup.sh
User=nobody
Group=nobody
PrivateTmp=true
```

#### Systemd Timer
```ini
[Unit]
Description=Run backup.service every day

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target
```

#### Sicherheit
- Verwenden Sie `PrivateTmp=true`, `User=nobody` und `Group=nobody` in der Systemd-Service-Datei.
- Verwenden Sie einen dedizierten SSH-Schlüssel nur für diese Aufgabe und beschränken Sie die Berechtigungen auf dem Remote-Rechner.

