### Projektaufgabe: Automatisiertes Backup-Script mit SCP und Systemd

#### Ziel
Entwicklung eines Bash-Scripts zur automatischen Sicherung von Verzeichnissen, das die Backups auf einen Remote-Rechner überträgt. Steuerung des Scripts durch Systemd und einer Timer Unit. Minimierung der Angriffsfläche durch geeignete Sicherheitsmaßnahmen.

#### Anforderungen

1. **Bash-Script**: Erstellung eines Bash-Scripts für die Durchführung von Backups.
2. **Remote-Übertragung**: Das Script soll die Backups auf einen Remote-Rechner übertragen.
3. **Erreichbarkeitsprüfung**: Vor der Übertragung soll die Erreichbarkeit des Remote-Rechners geprüft werden.
4. **SSH-Key-Passphrase**: Überlegung zur Verwendung einer Passphrase für den SSH-Schlüssel.
5. **Systemd Service**: Erstellung einer Systemd-Service-Datei zur Steuerung des Scripts.
6. **Systemd Timer**: Erstellung einer Systemd-Timer-Datei zur zeitlichen Steuerung des Services.
7. **Sicherheit**: Minimierung des `exposure value` durch geeignete Konfiguration des Services.

#### Bonusaufgabe
Überlegen Sie, wie eine SSH-Key-Passphrase sicher im System gespeichert und verwendet werden kann, ohne die Sicherheit zu kompromittieren.
